#!/usr/bin/python
import os.path
import re
import os
import markdown
import fileinput
from lxml import etree
from subprocess import call
import sys
import urllib2

greekWords = ['alpha', 'beta', 'gamma', 'delta',
              'epsilon', 'zeta', 'eta', 'theta',
              'iota', 'kappa', 'lambda', 'mu',
              'nu', 'xi', 'omicron', 'pi',
              'rho', 'sigma', 'tau', 'upsilon',
              'phi', 'chi', 'psi', 'omega'
             ]

def getGreekCharacter(word):
    if (word in greekWords):
        newWord = "$\\\\" + word + "$"
        return newWord
    return None

def replaceGreekCharacters(filename):
    for word in greekWords:
        command = "sed -i \'s/\\b" + word + "\\b/" + \
                   getGreekCharacter(word) + "/g\' " + filename
        os.system(command)


def parseIndex():
    # Check if index exists
    if os.path.isfile(indexFile):
        print("Index file exists")
    else:
        print("Index file does not exist")

    # Read data into string, replacing newline with nothing
    with open(indexFile, 'r') as myfile:
        data=myfile.read().replace('\n', '')

    # Extract links from body and insert into sourceFiles array
    sourceFiles  = re.findall( r'\((.*?)\)', data)
    print(sourceFiles)
    return sourceFiles


def getParentDirectoryName():
    return os.path.basename(os.getcwd()) + ".pdf"



def generatePDF(sourceFiles):
    # Check if pandoc is installed
    # TODO

    # Set pandoc options
    command = []
    command.append("pandoc")
    for file in sourceFiles:
        command.append(file)
    print("source files")
    print(sourceFiles)
    command.append("--toc")
    command.append("--toc-depth=1")
    command.append("--from")
    command.append("markdown+superscript+subscript")
    command.append("-o")
    command.append(getParentDirectoryName())
    call(command)

indexFile = sys.argv[1]
sourceFiles = parseIndex()
for filename in sourceFiles:
    replaceGreekCharacters(filename)
generatePDF(sourceFiles)

